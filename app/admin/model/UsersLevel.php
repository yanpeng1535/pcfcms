<?php
/***********************************************************
 * 会员等级
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Request;
class UsersLevel extends Common
{
    //列表
    public function tableData($post)
    {
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list = Db::name('users_level')
              ->field($tableWhere['field'])
              ->where($tableWhere['where'])
              ->order($tableWhere['order'])
              ->paginate($limit)->toArray();
        $pcfdata = $list['data'];
        $result = ['code' => 0, 'msg' => 'ok','count' =>$list['total'],'data' => $pcfdata];
        return $result;
    }

    protected function pcftableWhere($post)
    {
        $where = [];
        if (isset($post['level_name']) && $post['level_name'] != "") {
            $where[] = ['level_name', 'like', '%' . $post['level_name'] . '%'];
        }
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = "id desc";
        return $result;
    }

    //添加|编辑
    public function toAdd($data)
    {
        //判断是新增还是修改
        if (isset($data['id']) && !empty($data['id'])) {
            $add_data = [];
            $add_data['id'] = $data['id'];
            $add_data['level_name'] = $data['level_name'];
            $add_data['free_day_send'] = isset($data['free_day_send']) ? $data['free_day_send'] : '';
            $add_data['free_all_send'] = isset($data['free_all_send']) ? $data['free_all_send'] : '';
            $add_data['fee_day_top'] = isset($data['fee_day_top']) ? $data['fee_day_top'] : '';
            $add_data['fee_all_top'] = isset($data['fee_all_top']) ? $data['fee_all_top'] : '';
            $add_data['update_time'] = getTime();
            if(Db::name('users_level')->save($add_data)){
                $result = ['status' => true, 'msg' => '修改成功','url'=>Request::baseFile().'/userslevel/index'];
                return $result;
            }else{
                $result = ['status' => false, 'msg' => '修改失败'];
                return $result;
            } 
        } else {
            if(Db::name('users_level')->where('level_name','=',$data['level_name'])->find()){
                $result = ['status' => false, 'msg' => '已有相同名称！'];
                return $result;
            }
            $add_data = [];
            $add_data['level_name'] = $data['level_name'];
            $add_data['free_day_send'] = isset($data['free_day_send']) ? $data['free_day_send'] : '';
            $add_data['free_all_send'] = isset($data['free_all_send']) ? $data['free_all_send'] : '';
            $add_data['fee_day_top'] = isset($data['fee_day_top']) ? $data['fee_day_top'] : '';
            $add_data['fee_all_top'] = isset($data['fee_all_top']) ? $data['fee_all_top'] : '';
            $add_data['add_time'] = getTime();
            if (Db::name('users_level')->save($add_data)) {
                $result = ['status' => true, 'msg' => '添加成功','url'=>Request::baseFile().'/userslevel/index'];
                return $result;
            } else {
                $result = ['status' => false, 'msg' => '添加失败'];
                return $result;
            }  
        }
    }


}
