<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Request;
use think\facade\Session;
use think\facade\Cache;
use app\admin\model\Archives as Archivesmodel;
class Content extends Base
{
    public $Archivesmodel;
    public $admininfo;
    public $popedom;
    public function _initialize() 
    {
        parent::_initialize();
        $this->Archivesmodel = new Archivesmodel();
        $this->admininfo = session::get('admin_info');
        $ctl_act = 'content/index';
        $this->popedom = appfile_popedom($ctl_act);
    }

    public function index() 
    {
        //验证权限
        if(!$this->popedom["list"]){
            return $this->Notice(config('params.auth_msg.list'),true,3,false);
        }
        $channel_type = Db::name('channel_type')->where('status',1)->order('sort_order asc')->column('*','id');
        foreach ($channel_type as $key => $value) {
            if(6 == $value['id']){
              $channel_type[$key]['typeurl'] = pcfurl("/arctype/index",['current_channel'=> $value['id'],'lang'=> $value['lang']],true,false,true);
            }elseif(1 == $value['ifsystem'] && 6 != $value['id']){
              $channel_type[$key]['typeurl'] = pcfurl("/".$value['nid']."/index",['current_channel'=> $value['id'],'lang'=> $value['lang']],true,false,true);
            }else{
              $channel_type[$key]['typeurl'] = pcfurl("/custom/index",['current_channel'=> $value['id'],'lang'=> $value['lang']],true,false,true);
            }
            if($this->admininfo['role_id'] < 0 || $this->admininfo['auth_role_info']['only_oneself'] != 1){
                $channel_type[$key]['connum'] = Db::name('archives')->where(['status'=>1,'channel'=>$value['id']])->count();// 统计主文档数
                $channel_type[$key]['hiddennum'] = Db::name('archives')->where(['status'=>0,'channel'=>$value['id']])->count();// 统计隐藏主文档数
            }else{
                $channel_type[$key]['connum'] = Db::name('archives')->where(['status'=>1,'channel'=>$value['id'],'admin_id'=>$this->admininfo['admin_id']])->count();// 统计主文档数
                $channel_type[$key]['hiddennum'] = Db::name('archives')->where(['status'=>0,'channel'=>$value['id'],'admin_id'=>$this->admininfo['admin_id']])->count();// 统计隐藏主文档数    
            }
        }
        $this->assign('channeltypelist',$channel_type);
        return $this->fetch();
    }

    // 批量复制
    public function batch_copy()
    {
        if (Request::isAjax()) {
            $typeid = input('param.typeid/d');
            $aids = input('param.aids/s');
            $num = input('param.num/d');
            if (empty($typeid) || empty($aids)) {
                $result = ['status' => false, 'msg' => '参数有误，请联系技术支持'];
                return $result;
            } else if (empty($num)) {
                $result = ['status' => false, 'msg' => '复制数量至少一篇'];
                return $result;
            }
            // 获取复制栏目的模型ID
            $current_channel = Db::name('arctype')->where('id',$typeid)->value('current_channel');
            $pcfdata=[];
            $pcfdata[]=['aid','IN',$aids];
            $pcfdata[]=['channel','=',$current_channel];
            $aids = Db::name('archives')->where($pcfdata)->column('aid');
            // 复制文档处理
            $r = $this->Archivesmodel->batch_copy($aids, $typeid, $current_channel, $num);
            if($r){
                $channel_type = Db::name('channel_type')->field('nid,ifsystem,lang')->where('id',$current_channel)->find();
                if($channel_type["ifsystem"] == 1){
                    $gobackurl = pcfurl("/".$channel_type["nid"]."/index",['current_channel'=> $current_channel,'lang'=> $channel_type['lang']],true,true,true);
                }else{
                    $gobackurl = pcfurl("/custom/index",['current_channel'=> $current_channel,'lang'=> $channel_type['lang']],true,true,true);    
                }
                $result = ['status' => true, 'msg' => '操作成功','url'=>$gobackurl];
                return $result;
            }else{
                $result = ['status' => false, 'msg' => '操作失败'];
                return $result;
            }
        }
        $current_channel = input('param.current_channel/d', 0);
        $arctype_html = allow_release_arctype(0, [$current_channel],true,true);
        $this->assign('arctype_html', $arctype_html);
        return $this->fetch();
    }

    // 批量移动
    public function batch_move()
    {
        if (Request::isAjax()) {
            $post = input('param.');
            $typeid = !empty($post['typeid']) ? eyIntval($post['typeid']) : '';
            $aids = !empty($post['aids']) ? eyIntval($post['aids']) : '';
            if (empty($typeid) || empty($aids)) {
                $result = ['status' => false, 'msg' => '参数有误，请联系技术支持'];
                return $result;
            }
            $current_channel = Db::name('arctype')->where('id',$typeid)->value('current_channel');
            $pcfdata = [];
            if(is_string($aids)){
               $pcfdata[]=['aid','IN',$aids];
            }else{
               $pcfdata[]=['aid','=',$aids];
            }
            $pcfdata[]=['channel','=',$current_channel];
            $aids = Db::name('archives')->where($pcfdata)->column('aid');
            // 移动文档处理
            $update_data = [
                'typeid'    => $typeid,
                'update_time'   => getTime()
            ];
            $r = Db::name('archives')->where('aid','IN', $aids)->update($update_data);
            if($r){
                $channel_type = Db::name('channel_type')->field('nid,ifsystem,lang')->where('id',$current_channel)->find();
                if($channel_type["ifsystem"] == 1){
                    $gobackurl = pcfurl("/".$channel_type["nid"]."/index",['current_channel'=> $current_channel,'lang'=> $channel_type['lang']],true,true,true);
                }else{
                    $gobackurl = pcfurl("/custom/index",['current_channel'=> $current_channel,'lang'=> $channel_type['lang']],true,true,true);    
                }
                $result = ['status' => true, 'msg' => '操作成功','url'=>$gobackurl];
                return $result;
            }else{
                $result = ['status' => false, 'msg' => '操作失败'];
                return $result;
            }
        }
        $current_channel = input('param.current_channel/d', 0);
        $arctype_html = allow_release_arctype(0, [$current_channel],true,true);
        $this->assign('arctype_html', $arctype_html);
        return $this->fetch();
    }

    // 批量添加属性
    public function add_attr()
    {
        if (Request::isAjax()) {
            $post = input('param.');
            $paids = !empty($post['aids']) ? eyIntval($post['aids']) : '';
            if (empty($paids)) {
                $result = ['status' => false, 'msg' => '参数有误，请联系技术支持'];
                return $result;
            }
            if(is_string($paids)){
               $aids = Db::name('archives')->where('aid','IN', $paids)->column('aid');
            }else{
               $aids = Db::name('archives')->where('aid', $paids)->column('aid');
            }
            $update_data = ['update_time' => getTime()];
            if (isset($post['is_head']) && !empty($post['is_head'])){
                $update_data['is_head'] = 1;
            }
            if (isset($post['is_recom'])&& !empty($post['is_recom'])){
                $update_data['is_recom'] = 1;
            }
            if (isset($post['is_special']) && !empty($post['is_special'])){
                $update_data['is_special'] = 1;
            }
            if (isset($post['is_b']) && !empty($post['is_b'])){
                $update_data['is_b'] = 1;
            }
            if (isset($post['is_litpic']) && !empty($post['is_litpic'])){
                $update_data['is_litpic'] = 1;
            }
            $r = Db::name('archives')->where('aid','IN', $aids)->update($update_data);
            if($r){
                $result = ['status' => true, 'msg' => '操作成功'];
                return $result;
            }else{
                $result = ['status' => false, 'msg' => '操作失败'];
                return $result;
            }
        }
        return $this->fetch();
    }

    // 批量删除属性
    public function del_attr()
    {
        if (Request::isAjax()) {
            $post = input('post.');
            $paids = !empty($post['aids']) ? eyIntval($post['aids']) : '';
            if (empty($paids)) {
                $result = ['status' => false, 'msg' => '参数有误，请联系技术支持'];
                return $result;
            }
            if(is_string($paids)){
               $aids = Db::name('archives')->where('aid','IN', $paids)->column('aid');
            }else{
               $aids = Db::name('archives')->where('aid', $paids)->column('aid');
            }
            // 移动文档处理
            $update_data = ['update_time' => getTime()];
            if (isset($post['is_head']) && !empty($post['is_head'])){
                $update_data['is_head'] = 0;
            }
            if (isset($post['is_recom'])&& !empty($post['is_recom'])){
                $update_data['is_recom'] = 0;
            }
            if (isset($post['is_special']) && !empty($post['is_special'])){
                $update_data['is_special'] = 0;
            }
            if (isset($post['is_b']) && !empty($post['is_b'])){
                $update_data['is_b'] = 0;
            }
            if (isset($post['is_litpic']) && !empty($post['is_litpic'])){
                $update_data['is_litpic'] = 0;
            }
            $r = Db::name('archives')->where('aid','IN', $aids)->update($update_data);
            if($r){
                $result = ['status' => true, 'msg' => '操作成功'];
                return $result;
            }else{
                $result = ['status' => false, 'msg' => '操作失败'];
                return $result;
            }
        }
        return $this->fetch();
    }

    // 删除文档
    public function batch_del()
    {
        if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => 0, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => 0, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            } 
            $id_arr = input('param.del_id/a');
            $id_arr = eyIntval($id_arr);
            return $this->Archivesmodel->batch_del($id_arr);
        }       
    }

}