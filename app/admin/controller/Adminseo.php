<?php
/***********************************************************
 * URL配置
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Session;
use think\facade\Cache;
use think\facade\Request;
use think\facade\Cookie;
use app\common\logic\ArctypeLogic;
class Adminseo extends Base
{
    public $popedom;
    public function _initialize() {
        parent::_initialize();
        $ctl_act = Request::controller().'/index';
        $this->popedom = appfile_popedom($ctl_act);
    }

    //配置入口
    public function index()
    { 
        //验证权限
        if(!$this->popedom["list"]){
            return $this->Notice(config('params.auth_msg.list'),true,3,false);
        }
        $seo_pseudo_list = get_seo_pseudo_list();
        $this->assign('seo_pseudo_list', $seo_pseudo_list);
        $this->assign('config', sysConfig('seo'));
        return $this->fetch();
    }

    public function add()
    { 
        $domain = Request::baseFile().'/adminseo/index';
        //验证修改权限
        if(!$this->popedom["modify"]){
            if(config('params.auth_msg.test')){
                $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                return $result;
            }else{
                $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                return $result;                    
            }
        }
        $param = input('param.');
        sysConfig('seo',$param);
        Cache::clear();
        $admin_temp = glob(WWW_ROOT.'runtime/admin/temp/'.'*.php');
        array_map('unlink',$admin_temp);
        $result = ['status'=> true,'msg'=> '操作成功','url'=>$domain];
        return $result;
    }

}
