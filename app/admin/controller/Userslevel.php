<?php
/***********************************************************
 * 会员等级
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Request;
use app\admin\model\UsersLevel as UsersLevelModel;
class Userslevel extends Base
{
    public $UsersLevelModel;
    public $popedom = '';
    public function _initialize() {
        parent::_initialize();
        $this->UsersLevelModel = new UsersLevelModel();
        $ctl_act = strtolower(Request::controller().'/index');
        $this->popedom = appfile_popedom($ctl_act);
    }

    // 列表
    public function index(){ 
        if(!$this->popedom["list"]){
            return $this->Notice(config('params.auth_msg.list'),true,3,false);
        }
        if (Request::isAjax()) {
            return $this->UsersLevelModel->tableData(input('param.'));
        }
        return $this->fetch();
    }

    //添加
    public function add(){
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        if (Request::isPost()) {
            if(!$this->popedom["add"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.add')];
                    return $result;                    
                }
            }
            return $this->UsersLevelModel->toAdd(input('param.'));
        }
        return $this->fetch();
    }
    
    // 编辑
    public function edit(){
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        if (Request::isPost()) {
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }
            return $this->UsersLevelModel->toAdd(input('param.'));
        }
        $info = Db::name('users_level')->where('id',input('param.id/d'))->find();
        if(!$info)$info = "";
        $this->assign('info', $info);
        return $this->fetch();
    }

    // 删除
    public function del(){
        if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            } 
            $id = input('param.del_id/d');
            if(Db::name('users')->where("level_id",$id)->find()){
                $result = ['status' => false, 'msg' => '已有会员使用'];
                return $result; 
            }
            if (Db::name('users_level')->where("id",$id)->delete()) {
                $result = ['status' => true, 'msg' => '删除成功'];
                return $result;
            } else {
                $result = ['status' => false, 'msg' => '删除失败'];
                return $result;
            }
            return $result;
        }       
    }

    // 批量删除
    public function batch_del(){
        if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            } 
            $id_arr = input('param.del_id/a');
            $id_arr = eyIntval($id_arr);
            foreach ($id_arr as $key => $value) {
                $pcf = Db::name('users')->where("level_id",$value)->find();
                $level_name = Db::name('users_level')->where("id",$value)->value('level_name');
                if($pcf){
                    $result = ['status' => false, 'msg' => $level_name.'已有会员使用'];
                    return $result; 
                }
            }
            if(is_array($id_arr) && !empty($id_arr)){
                foreach ($id_arr as $key => $val) {
                   Db::name('users_level')->where('id',$val)->delete();
                }
                $result = ['status' => true, 'msg' => '删除成功'];
                return $result;
            } else {
                $result = ['status' => false, 'msg' => '参数有误'];
                return $result;
            }
        }       
    }

}
