<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\common\logic;
use lunzi\TpSms;
use app\common\logic\AlismsLogic;
class SmsLogic 
{
    public function send_msg($to='', $code, $TemplateCode){
        $result = ['code'=>0 , 'msg'=>'该功能待开放'];
        // 是否填写短信配置
        $sms_config = sysConfig('sms');
        if($sms_config['sms_syn_weapp'] == 1 || !empty($sms_config['sms_syn_weapp'])){
            unset($sms_config['sms_content']);
            unset($sms_config['sms_test']);
            foreach ($sms_config as $key => $val) {
                if (empty($val)) {
                    return array('code'=>0 , 'msg'=>'该功能待开放，网站管理员尚未完善短信配置！');
                }
            }
        }else{
            return array('code'=>0 , 'msg'=>'后台短信功能没有开启！');
        }
        // 创信短信
        if($sms_config['sms_platform'] == 1){
            $tpSms = new TpSms();
            $sms_appkey = $sms_config['sms_appkey'];
            $sms_secretkey = $sms_config['sms_secretkey'];
            if(isset($sms_config['sms_content']) && !empty($sms_config['sms_content'])){
               $content = $sms_config['sms_content'];//短信内容
            }else{
               $content ="【pcfcms】验证码为：".$code."。尊敬的客户，请在5分钟之内输入该手机动态验证码，过期自动失效。";//短信内容 
            }
            $json = $tpSms->sendsms('1555',$sms_appkey,$sms_secretkey,$to,$content);
            $result1 = json_decode($json, true);
            if($result1['returnstatus'] = "success" && $result1['message'] = "ok"){
                $result = ['code'=>1 , 'msg'=>'发送成功'];
            }else{
                $result = ['code'=>0 , 'msg'=>'发送失败！'];
            }
        }
        // 阿里云短信
        if($sms_config['sms_platform'] == 2){
            
            $params = [];

            // *** 需用户填写部分 ***
            // fixme 必填：是否启用https
            $security = false;

            // fixme 必填: 请参阅 https://ak-console.aliyun.com/ 取得您的AK信息
            $accessKeyId = $sms_config['sms_appkey'];
            $accessKeySecret = $sms_config['sms_secretkey'];

            // fixme 必填: 短信接收号码
            $params["PhoneNumbers"] = $to;

            // fixme 必填: 短信签名，应严格按"签名名称"填写，请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/sign
            $params["SignName"] = "pcfcms系统";

            // fixme 必填: 短信模板Code，应严格按"模板CODE"填写, 请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/template
            if($TemplateCode){
                $params["TemplateCode"] = $TemplateCode;
            }else{
                $params["TemplateCode"] = "SMS_214520607";
            }

            // fixme 可选: 设置模板参数, 假如模板中存在变量需要替换则为必填项
            if($code){
                $params['TemplateParam'] = Array (
                    "code" => $code,
                    //"product" => "阿里通信"
                );                
            }

            // fixme 可选: 设置发送短信流水号
            //$params['OutId'] = "12345";

            // fixme 可选: 上行短信扩展码, 扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段
            //$params['SmsUpExtendCode'] = "1234567";

            // *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
            if(!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
                $params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
            }

            // 初始化SignatureHelper实例用于设置参数，签名以及发送请求
            $helper = new AlismsLogic();
            $params = array_merge($params, ["RegionId" => "cn-hangzhou","Action" => "SendSms","Version" => "2017-05-25"]);

            // 此处可能会抛出异常，注意catch
            $content = $helper->request($accessKeyId,$accessKeySecret,"dysmsapi.aliyuncs.com",$params,$security);
            if($content->code = "ok" && $content->message = "ok"){
                $result = ['code'=>1 , 'msg'=>'发送成功'];
            }else{
                $result = ['code'=>0 , 'msg'=>'发送失败！'];
            }
        }
        return $result;
    }



}
